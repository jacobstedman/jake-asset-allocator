from visuals import LineChart

def line_chart(data, sell_directly, threshold_strategy, lookbehind, threshold, start_year):
    chart = LineChart()
    chart.x = data.months
    chart.y1 = data.values
    chart.y2 = sell_directly.values
    chart.y3 = threshold_strategy.values
    chart.show([
        f'Buy and hold',
        f'Sell directly ({lookbehind}m lookbehind)',
        f'Threshold strategy ({lookbehind}m lookbehind, {100*threshold}% threshold)'
    ])

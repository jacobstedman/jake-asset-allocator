import requests
import datetime
import json

def fetch_and_save(filename):
    # ajax request from http://www.nasdaqomxnordic.com/indexes/historical_prices?Instrument=SE0000744195 (OMXSPI)
    url = "http://www.nasdaqomxnordic.com/webproxy/DataFeedProxy.aspx?SubSystem=History&Action=GetChartData&inst.an=id%2Cnm%2Cfnm%2Cisin%2Ctp%2Cchp%2Cycp&" + \
        "FromDate=1990-01-01&" + \
        "ToDate=" + str(datetime.date.today()) + "&" + \
        "json=true&timezone=CET&showAdjusted=false&app=%2Findexes%2Fhistorical_prices-HistoryChart&DefaultDecimals=false&Instrument=SE0000744195"
    headers = {
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1',
    }

    r = requests.get(url, headers=headers)
    with open(filename, "w") as f:
        f.write(r.text)

def load(filename):
    with open(filename, "r") as f:
        data = json.load(f)
        return data["data"][0]["chartData"]["cp"]

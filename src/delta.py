
def delta(threshold_strategy, ym=None):
    if ym == None:
        # assume we are doing this early in month n, so always look at month n-1 and compare to n-2
        curr = -2 # last month in set
        prev = -3 # month before that
    else:
        i = threshold_strategy.find_ym(ym)
        if i == -1:
            raise Exception(f"Month {ym} not found in data set.")
        curr = i
        prev = i-1

    prev_month = threshold_strategy[prev].ym
    curr_month = threshold_strategy[curr].ym
    prev_strategy = threshold_strategy[prev].stay_with_equity
    curr_strategy = threshold_strategy[curr].stay_with_equity
    same_strategy = prev_strategy == curr_strategy
    buy_signal = threshold_strategy[curr].buy_signal
    sell_signal = threshold_strategy[curr].sell_signal
    v = threshold_strategy[curr].v
    trend_line = threshold_strategy[curr].trend_line
    trend_line_source = threshold_strategy[curr].trend_line_source
    range = threshold_strategy[curr].range_as_str
    lower = round(threshold_strategy[curr].lower_range, 0)
    upper = round(threshold_strategy[curr].upper_range, 0)

    # First sentence
    s = f"After {prev_month}, the recommendation was to {'' if prev_strategy else 'NOT '}be in the equity market, "
    s += "and" if same_strategy else "but"
    s += f" after {curr_month} the recommendation is {'the same' if same_strategy else 'to reverse'}: "
    if curr_strategy and not prev_strategy:
        s += "to enter"
    elif curr_strategy and prev_strategy:
        s += "to stay in"
    elif not curr_strategy and not prev_strategy:
        s += "to stay out of"
    elif not curr_strategy and prev_strategy:
        s += "to leave"
    s += " the equity market.\n\n"

    # Second sentence
    s_threshold = f"In {curr_month}, the average index value was {v}, and the threshold range was {range} (center of {trend_line} "
    s_threshold += f"based on average of {trend_line_source}), which we "
    if prev_strategy:
        s += f"Rationale: As we entered {curr_month}, we were in the market. "
        s += s_threshold
        s += "see" if sell_signal else "do not see"
        s += " as a sell signal"
        s += f" (as {v} is {'' if v < lower else 'NOT '}below {lower})."
    else:
        s += f"Rationale: As we entered {curr_month}, we were out of the market. "
        s += s_threshold
        s += "see" if buy_signal else "do not see"
        s += " as a buy signal"
        s += f" (as {v} is {'' if v > upper else 'NOT '}above {upper})."
    s += "\n"

    print(s)

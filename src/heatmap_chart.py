from visuals import HeatmapChart

def heatmap(data, sell_directly, threshold_strategy, lookbehind, threshold, start_year):

    heatmap_chart_1 = HeatmapChart(f'Sell directly (from {start_year}, {lookbehind}m lookbehind)')
    for month in sell_directly:
        heatmap_chart_1.x.append(month.y)
        heatmap_chart_1.y.append(month.m)
        heatmap_chart_1.values.append(month.month_below_trend_line)
    heatmap_chart_1.save()

    heatmap_chart_2 = HeatmapChart(f'Threshold (from {start_year}, {lookbehind}m lookbehind, {threshold} threshold)')
    for month in threshold_strategy:
        heatmap_chart_2.x.append(month.y)
        heatmap_chart_2.y.append(month.m)
        heatmap_chart_2.values.append(0 if month.stay_with_equity == True or month.stay_with_equity is None else 1)
    heatmap_chart_2.save()

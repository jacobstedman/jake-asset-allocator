import abc

from month import Month
from month_list import MonthList

class StrategyMonth(Month):

    def __init__(self, y, m, value):
        super().__init__(y, m, value)
        self.trend_line = None
        self._strategy_v = None

    def __str__(self):
        part1 = f"{self.ym} {self.strategy_v}"
        part2 = f' (' + str(round(100*self.change,1)) + '%)' if self.change is not None else ''
        part3 = f' (' + str(round(self.trend_line,0)) + ')' if self.trend_line is not None else ''
        return f"{part1}{part2}{part3}"

    @property
    def strategy_v(self):
        return self._strategy_v if self._strategy_v is not None else self.v

    @strategy_v.setter
    def strategy_v(self, v):
        self._strategy_v = v

class Strategy(MonthList):

    def __init__(self, month_class, start_year, end_year, trend_lookbehind):
        super().__init__(month_class, start_year, end_year)
        self.trend_lookbehind = trend_lookbehind

    @classmethod
    def clone(cls, month_class, old_month_list, trend_lookbehind):
        new_month_list = cls(month_class, old_month_list.start_year, old_month_list.end_year, trend_lookbehind)
        new_month_list.data = [month_class(item.y, item.m, item.v) for item in old_month_list.data]
        new_month_list.calc_month_on_month_change()
        return new_month_list

    def calc_trend_line(self):
        for i, item in enumerate(self.data):
            if i-self.trend_lookbehind >= 0:
                # remember, stop value is not included, so need to +1. And we want lookbehind to include the current value.
                months_in_trend = [item.v for item in self.data[i-self.trend_lookbehind+1:i+1]]
                item.trend_line = sum(months_in_trend)/len(months_in_trend)
                item.trend_line_source = ', '.join([str(item) for item in months_in_trend])
                #print(f"Trendline for {item.ym} calculated based on {self.data[i-self.trend_lookbehind+1].ym} to {self.data[i].ym} ({len(months_in_trend)} value(s))")

    def preprocess(self):
        self.calc_trend_line()

    def compute(self):
        self.preprocess()
        for i in range(len(self.data)):
            if i >= 1:
                if self.stay_with_equity(i):
                    self.data[i].strategy_v = self.data[i-1].strategy_v * (1+self.data[i].change)
                else:
                    self.data[i].strategy_v = self.data[i-1].strategy_v
        return self

    @property
    def values(self):
        return [ month.strategy_v for month in self.data ]

    @property
    def last_value(self):
        return self.data[-1].strategy_v

    @property
    def growth_over_period(self):
        return self.data[-1].strategy_v/self.data[0].strategy_v-1

    @abc.abstractmethod
    def stay_with_equity(self, idx):
        pass

import unittest

from month import Month
from month_list import MonthList
from strategy import Strategy, StrategyMonth

class AlwaysEquityStrategy(Strategy):

    def stay_with_equity(self, idx):
        return True

class NeverEquityStrategy(Strategy):

    def stay_with_equity(self, idx):
        return False

class MonthListTestCase(unittest.TestCase):

    def test_ml_init(self):
        ml = MonthList(Month, 2020, 2020)
        ml.append(Month(2020, 1, 100))
        ml.append(Month(2020, 2, 110))
        ml.append(Month(2020, 3, 120))
        self.assertEqual(str(ml), "2020-01 100\n2020-02 110\n2020-03 120")

    def test_ml_calc_month_on_month_change(self):
        ml = MonthList(Month, 2020, 2020)
        ml.append(Month(2020, 1, 100))
        ml.append(Month(2020, 2, 110))
        ml.append(Month(2020, 3, 120))
        ml.calc_month_on_month_change()
        self.assertEqual(str(ml), "2020-01 100\n2020-02 110 (10.0%)\n2020-03 120 (9.1%)")

class StrategyTestCase(unittest.TestCase):

    def test_strategy_init(self):
        INPUT_VALUES = [100, 110, 120, 130, 140]
        EXPECTED_AVERAGES = [None, None, 110, 120, 130]
        s = AlwaysEquityStrategy(StrategyMonth, 2020, 2020, 3)
        for i, v in enumerate(INPUT_VALUES):
            s.append(StrategyMonth(2020, i+1, v))
        s.calc_month_on_month_change()
        s.calc_trend_line()
        for i, v in enumerate(EXPECTED_AVERAGES):
            self.assertEqual(s[i].trend_line, v)

    def test_strategy_clone(self):
        INPUT_VALUES = [100, 110, 120, 130, 140]
        EXPECTED_AVERAGES = [None, None, 110, 120, 130]
        ml = MonthList(Month, 2020)
        for i, v in enumerate(INPUT_VALUES):
            ml.append(StrategyMonth(2020, i+1, v))
        ml.calc_month_on_month_change()
        s = AlwaysEquityStrategy.clone(StrategyMonth, ml, 3)
        s.calc_trend_line()
        for i, v in enumerate(EXPECTED_AVERAGES):
            self.assertEqual(s[i].trend_line, v)

    def test_always_equity(self):
        INPUT_VALUES = [100, 110, 120, 130, 140]
        EXPECTED_OUTPUT = [100, 110, 120, 130, 140]
        s = AlwaysEquityStrategy(StrategyMonth, 2020, 2020, 3)
        for i, v in enumerate(INPUT_VALUES):
            s.append(StrategyMonth(2020, i+1, v))
        s.calc_month_on_month_change()
        s.calc_trend_line()
        s.compute()
        for i, strategy_v in enumerate(EXPECTED_OUTPUT):
            self.assertEqual(int(s[i].strategy_v), int(strategy_v))

    def test_never_equity(self):
        INPUT_VALUES = [100, 110, 120, 130, 140]
        EXPECTED_OUTPUT = [100, 100, 100, 100, 100]
        s = NeverEquityStrategy(StrategyMonth, 2020, 2020, 3)
        for i, v in enumerate(INPUT_VALUES):
            s.append(StrategyMonth(2020, i+1, v))
        s.calc_month_on_month_change()
        s.calc_trend_line()
        s.compute()
        for i, strategy_v in enumerate(EXPECTED_OUTPUT):
            self.assertEqual(int(s[i].strategy_v), int(strategy_v))

unittest.main()

from month_list import MonthList
from strategy import StrategyMonth, Strategy

class SellDirectlyMonth(StrategyMonth):

    def __str__(self):
        part1 = f"{self.ym} {self.strategy_v}"
        part2 = f' (' + str(round(100*self.change,1)) + '%)' if self.change is not None else ''
        part3 = f' (' + str(round(self.trend_line,0)) + ')' if self.trend_line is not None else ''
        part4 = f' (L)' if self.month_below_trend_line else ''
        return f"{part1}{part2}{part3}{part4}"

    @property
    def month_below_trend_line(self):
        return self.trend_line is not None and self.v < self.trend_line

class SellDirectlyStrategy(Strategy):

    def stay_with_equity(self, idx):
        return not self.data[idx-1].month_below_trend_line

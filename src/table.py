from prettytable import PrettyTable

def table(data, sell_directly, threshold):

    table = PrettyTable()
    table.field_names = ["Month", "Historical", "Change", "Yearly avg", "Below", "Sell directly", "Threshold in market", "Threshold",]
    for i in range(len(data)):
        grid_row = []
        grid_row.append(data[i].ym)
        grid_row.append(data[i].v)
        grid_row.append(str(round(100*data[i].change,1))+'%' if data[i].change is not None else '')
        grid_row.append("trend " + str(int(sell_directly[i].trend_line)) if sell_directly[i].trend_line is not None else '')
        grid_row.append("BELOW TREND" if sell_directly[i].month_below_trend_line else '')
        grid_row.append("sd " + str(int(sell_directly[i].strategy_v)))
        in_market = ', '.join([j for j in [
            'BUY' if threshold[i].buy_signal else None,
            'SELL' if threshold[i].sell_signal else None,
            'STAY WITH EQUITY' if threshold[i].stay_with_equity else 'STAY OUT OF EQUITY'] if j is not None])
        grid_row.append(in_market)
        grid_row.append("t2 " + str(int(threshold[i].strategy_v)))
        table.add_row(grid_row)
    print(table)

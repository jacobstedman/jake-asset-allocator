import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

class LineChart():

    def __init__(self):
        self.x = []
        self.y1 = []
        self.y2 = []
        self.y3 = []

    def show(self, labels):
        df=pd.DataFrame(
            {
                'x': self.x,
                'y1': self.y1,
                'y2': self.y2,
                'y3': self.y3,
            }
        )
        fig, ax = plt.subplots()
        plt.plot( 'x', 'y1', '', data=df, marker='', color='skyblue', linewidth=2, label=labels[0])     # marker='o', markerfacecolor='blue', markersize=12, linewidth=4
        plt.plot( 'x', 'y2', '', data=df, marker='', color='olive', linewidth=2, label=labels[1])
        plt.plot( 'x', 'y3', '', data=df, marker='', color='olive', linewidth=2, linestyle='dashed', label=labels[2])
        ax.xaxis.set_ticks([label for label in self.x if label.endswith('01')])
        ax.xaxis.set_ticklabels([label[2:4] for label in self.x if label.endswith('01')])
        plt.legend()
        plt.show()

class HeatmapChart():

    def __init__(self, title):
        self.x = []
        self.y = []
        self.values = []
        self.title = title

    def save(self):
        df = pd.DataFrame({'x': self.x, 'y': self.y, 'values': self.values })
        df_wide = df.pivot_table( index='y', columns='x', values='values' )
        ax = plt.axes()
        ax.set_title(self.title)
        p2 = sns.heatmap(df_wide, ax=ax, cbar=False, cmap="Greens")
        #plt.show()
        plt.savefig(f"images/{self.title}.png", dpi=600)
        plt.close()

class ScatterChart():

    def __init__(self):
        self.xlabel = None
        self.ylabel = None
        self.fig, self.ax = plt.subplots()

    def new_segment(self):
        self.x = []
        self.y = []
        self.area = []

    def draw_segment(self, color, color_name):
        self.ax.scatter(self.x, self.y, c=[color]*len(self.x), s=self.area, label=color_name)       # , alpha=0.5

    def show(self):
        if self.xlabel:
            plt.xlabel(self.xlabel)
        if self.ylabel:
            plt.ylabel(self.ylabel)
        plt.legend()
        plt.show()

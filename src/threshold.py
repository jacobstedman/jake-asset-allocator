from month_list import MonthList
from strategy import Strategy, StrategyMonth

class ThresholdMonth(StrategyMonth):

    def __init__(self, y, m, value):
        super().__init__(y, m, value)
        self.stay_with_equity = None
        self.threshold = None

    @property
    def buy_signal(self):
        return self.v > self.upper_range if self.trend_line is not None else None

    @property
    def sell_signal(self):
        return self.v < self.lower_range if self.trend_line is not None else None

    @property
    def lower_range(self):
        return self.trend_line*(1-self.threshold)

    @property
    def upper_range(self):
        return self.trend_line*(1+self.threshold)

    @property
    def range_as_str(self):
        return f"{round(self.lower_range, 0)}-{round(self.upper_range, 0)}"

    def __str__(self):
        part1 = f"{self.ym} {self.strategy_v}"
        part2 = f' (' + str(round(100*self.change,1)) + '%)' if self.change is not None else ''
        part3 = f' ({self.range_as_str})' if self.trend_line is not None else ''
        part4 = f' (B)' if self.buy_signal else ''
        part5 = f' (S)' if self.sell_signal else ''
        return f"{part1}{part2}{part3}{part4}{part5}"

class ThresholdStrategy(Strategy):

    def set_threshold(self, threshold):
        for month in self.data:
            month.threshold = threshold
        return self

    def calc_signals(self):
        for i in range(len(self.data)):
            if i >= 1:
                # if we are in the market, look for sell signals, otherwise, look for buy signals
                if self.data[i-1].stay_with_equity:
                    self.data[i].stay_with_equity = False if self.data[i].sell_signal else True
                else:
                    self.data[i].stay_with_equity = self.data[i].buy_signal

    def preprocess(self):
        super().preprocess()
        self.calc_signals() # need to pre-cook a few things

    def stay_with_equity(self, i):
        return self.data[i-1].stay_with_equity or self.data[i-1].stay_with_equity is None

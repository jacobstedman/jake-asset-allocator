import datetime

class MonthList:

    def __init__(self, month_class, start_year, end_year):
        self.data = []
        self.month_class = month_class
        self.start_year = start_year
        self.end_year = end_year

    def find_ym(self, ym):
        for i, m in enumerate(self.data):
            if m.ym == ym:
                return i
        return -1

    def load_from_nasdaq_data(self, nasdaq_data):
        # step 1: convert from epoch ms to dates
        nasdaq_chart_data_by_date = [ [datetime.datetime.fromtimestamp(item[0]//1000.0).date(), item[1] ] for item in nasdaq_data ]
        dont_care_about_period = self.start_year is None or self.end_year is None

        # step 2: structure into dict of dict of years and months
        structured_items = {}
        for item in nasdaq_chart_data_by_date:
            y = item[0].year
            m = item[0].month
            d = item[0].day
            price = item[1]
            if dont_care_about_period or (y >= self.start_year and y <= self.end_year):
                if not y in structured_items:
                    structured_items[y] = {}
                if not m in structured_items[y]:
                    structured_items[y][m] = []
                structured_items[y][m].append(price)        # assume one data point for each day

        # step 3: calculate averages for each month
        self.data = [ self.month_class(y, m, int(sum(structured_items[y][m])//len(structured_items[y][m]))) for y in structured_items for m in structured_items[y] ]

        # step 4: calculate month-on-month change
        self.calc_month_on_month_change()

    def calc_month_on_month_change(self):
        for i in range(len(self.data)):
            if i > 0:
                self.data[i].change = self.data[i].v / self.data[i-1].v - 1

    def __str__(self):
        return '' + '\n'.join([str(item) for item in self.data]) + ''

    def __getitem__(self, idx):
        return self.data[idx]

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        self.curr_idx = -1      # we'll increment before accessing, so start at -1
        return self

    def __next__(self):
        self.curr_idx += 1
        if self.curr_idx > len(self.data)-1: raise StopIteration
        return self.data[self.curr_idx]

    def append(self, month):
        return self.data.append(month)

    @property
    def months(self):
        return [ month.ym for month in self.data ]

    @property
    def every_12_month(self):
        return [ int(month.y) if month.m==1 else 0 for month in self.data ]

    @property
    def values(self):
        return [ month.v for month in self.data ]

    @property
    def last_value(self):
        return self.data[-1].v

    @property
    def growth_over_period(self):
        return self.data[-1].v/self.data[0].v-1

    @property
    def growth_by_year(self):
        t = len(set([item.y for item in self.data]))
        return (self.growth_over_period+1)**(1/t)-1

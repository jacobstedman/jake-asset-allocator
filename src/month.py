
class Month:

    def __init__(self, y, m, value):
        self.y = y
        self.m = m
        self.v = value
        self.change = None

    def __str__(self):
        part1 = f"{self.ym} {self.v}"
        part2 = f' (' + str(round(100*self.change,1)) + '%)' if self.change is not None else ''
        return f"{part1}{part2}"

    @property
    def ym(self):
        return f"{self.y}-{self.m:02}"

import sys
import fetch
import datetime

from month import Month
from month_list import MonthList
from sell_directly import SellDirectlyStrategy, SellDirectlyMonth
from threshold import ThresholdStrategy, ThresholdMonth
from line_chart import line_chart
from heatmap_chart import heatmap
from table import table
from optimize import optimize
from delta import delta

FILENAME = 'data/asset_data.json'
LOOKBEHIND = 2
THRESHOLD = 0.01

if len(sys.argv) == 2 and sys.argv[1] == 'fetch':
    fetch.fetch_and_save(FILENAME)
    print(f"Fetched and saved as '{FILENAME}'...")
elif len(sys.argv) in (2, 3) and sys.argv[1] in ('historical', 'heatmap', 'table'):
    START_YEAR = 1990 if len(sys.argv) == 2 else int(sys.argv[2])
    END_YEAR = datetime.datetime.now().year
    raw_data = fetch.load(FILENAME)
    data = MonthList(Month, START_YEAR, END_YEAR)
    data.load_from_nasdaq_data(raw_data)
    sell_directly = SellDirectlyStrategy.clone(SellDirectlyMonth, data, LOOKBEHIND).compute()
    threshold_strategy = ThresholdStrategy.clone(ThresholdMonth, data, LOOKBEHIND).set_threshold(THRESHOLD).compute()
    if sys.argv[1] == 'historical':
        line_chart(data, sell_directly, threshold_strategy, LOOKBEHIND, THRESHOLD, START_YEAR)
    elif sys.argv[1] == 'table':
        table(data, sell_directly, threshold_strategy)
    elif sys.argv[1] == 'heatmap':
        heatmap(data, sell_directly, threshold_strategy, LOOKBEHIND, THRESHOLD, START_YEAR)
elif len(sys.argv) == 2 and sys.argv[1] == 'optimize':
    raw_data = fetch.load(FILENAME)
    optimize(raw_data)
elif len(sys.argv) in (2, 3) and sys.argv[1] == 'delta':
    raw_data = fetch.load(FILENAME)
    data = MonthList(Month, None, None)
    data.load_from_nasdaq_data(raw_data)
    threshold_strategy = ThresholdStrategy.clone(ThresholdMonth, data, LOOKBEHIND).set_threshold(THRESHOLD).compute()
    delta(threshold_strategy, None if len(sys.argv) == 2 else sys.argv[2])
else:
    sys.exit("Syntax: python3 main.py [fetch|historical|heatmap|optimize|delta] [start_year] [month]")

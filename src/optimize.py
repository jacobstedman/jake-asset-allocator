import itertools

from sell_directly import SellDirectlyStrategy, SellDirectlyMonth
from threshold import ThresholdStrategy, ThresholdMonth
from prettytable import PrettyTable
from month_list import MonthList
from month import Month
from visuals import ScatterChart

def add_entry(table, items, start_year, end_year, lookbehind, threshold, last_value, cagr):
    growth = round(100*cagr,1)
    s_threshold = threshold if isinstance(threshold, str) else "T"+str(threshold),
    item = [f"{start_year}-{end_year}", str(lookbehind), str(s_threshold), int(last_value), growth]
    table.add_row(item)
    if threshold not in items:
        items[threshold] = {}
    if lookbehind not in items[threshold]:
        items[threshold][lookbehind] = { }
    items[threshold][lookbehind][start_year] = growth

def optimize(raw_data):
    items = {}
    PERIODS = [[1990, 2020], [2000, 2020], [2010, 2020]]        # Start years need to be unique

    # Generate and write tables
    for period in PERIODS:
        table = PrettyTable()
        table.field_names = ["Period", "Lookbehind", "Threshold", "Last value", "CAGR (%)" ]
        table.sortby = "CAGR (%)"
        data = MonthList(Month, period[0], period[1])
        data.load_from_nasdaq_data(raw_data)
        add_entry(table, items, period[0], period[1], '-', "Buy-and-hold", data.last_value, data.growth_by_year)
        for lookbehind in [1, 2, 3, 4, 5, 6, 9, 12, 18]:
            sell_directly = SellDirectlyStrategy.clone(SellDirectlyMonth, data, lookbehind).compute()
            add_entry(table, items, period[0], period[1], lookbehind, "SD", sell_directly.last_value, sell_directly.growth_by_year)
            for threshold in [0, 0.01, 0.02, 0.03]:
                threshold_strategy = ThresholdStrategy.clone(ThresholdMonth, data, lookbehind).set_threshold(threshold).compute()
                add_entry(table, items, period[0], period[1], lookbehind, threshold, threshold_strategy.last_value, threshold_strategy.growth_by_year)
        print(table)

    # Generate and show scatter chart
    PERIOD_X = PERIODS[0]
    PERIOD_Y = PERIODS[2]
    scatter = ScatterChart()
    colors = itertools.cycle(["black", "darkred", "gold", "yellow", "dodgerblue", "slategrey"])
    scatter.xlabel = f'CAGR {PERIOD_X[0]}-{PERIOD_X[1]}'
    scatter.ylabel = f'CAGR {PERIOD_Y[0]}-{PERIOD_Y[1]}'
    for threshold in items:
        scatter.new_segment()
        scatter.x = [items[threshold][lookbehind][PERIOD_X[0]] for lookbehind in items[threshold]]
        scatter.y = [items[threshold][lookbehind][PERIOD_Y[0]] for lookbehind in items[threshold]]
        scatter.area = [get_size(lookbehind) for lookbehind in items[threshold]]
        scatter.draw_segment(next(colors), threshold)
    scatter.show()

def get_size(lookbehind):
    if not str(lookbehind).isdigit():
        return 1000
    else:
        return lookbehind*50

def get_color(threshold):
    if threshold == 'Buy-and-hold':
        return 1
    elif threshold == 'SD':
        return 2
    else:
        return 3+threshold
